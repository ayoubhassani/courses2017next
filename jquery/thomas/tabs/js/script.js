$(document).ready(function() {
    /*
    var radio = $("ul li:first a").attr('href', "lemonde.fr");
    console.log($("ul li:first a").attr('href'));
    $("ul li:first a").attr('thomas', "nolo.fr");
    console.log($("ul li:first a").attr('thomas'));
    */
    var a = $('ul li');
    $('ul li:first-child').addClass('active');
    var lienActif = $('ul li:first-child').children().attr('href');
    $("div"+lienActif).css('display', 'block');

    a.click( function () {
        var liActif = $('li.active').children().attr('href');
        $('li.active').removeClass('active');

        // ou : $('div'+liActif).hide(); => méthode jQuery qui joue sur l'attribut display
        // ou : $('div'+liActif).slideToggle()
        $('div'+liActif).hide();


        $(this).addClass('active');
        var attribut = $(this).children().attr('href');
        // ou : $('div'+liActif).show(); => méthode jQuery qui joue sur l'attribut display
        // ou : $('div'+liActif).css('display', 'block');
        $('div'+attribut).slideToggle();

    });
});
