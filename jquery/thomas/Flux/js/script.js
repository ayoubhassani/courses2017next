function slick() {
    $("#slider").slick({

        // normal options...
        infinite: false,
        autoplay:true,
        speed:500,

        // the magic
        responsive: [{

            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                infinite: true
            }

        }, {

            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                dots: true
            }

        }, {

            breakpoint: 300,
            settings: "unslick" // destroys slick

        }]
    });
}

$(document).ready(slick);