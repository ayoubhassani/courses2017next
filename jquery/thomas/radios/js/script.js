$(document).ready(function() {
   $('input[type=radio]').each(function () {
       $(this).hide();
       $('<img src="img/radio_off.png" class="customRadios">').insertBefore($(this));
   });

    $('.customRadios').click(function () {
        //$('input[type=radio]:checked').prop("checked", false)
        var radio = $(this).next();
        var name = radio.attr("name");
        console.log(name);
        $('input[name='+name+']:checked').prev().attr('src', "img/radio_off.png");

        if(radio.is(':checked')) {
            radio.prop("checked", false);
            this.src = "img/radio_off.png"; // En mode JavaScript
        } else {
            radio.prop("checked", true);
            $(this).attr('src',"img/radio_on.png"); // En mode jQuery
        }
    });

    $('input[type=submit]').click(function (e) {
        e.preventDefault(); // On arrête par défault le comportement du formulaire
        console.log($("form")[0].checkValidity());
        var res=[];
        if(
            ($('input[name=sexe]:checked').length === 1) &&
            ($('input[name=casier]:checked').length === 1)
        ) {
            console.log($('input[name=sexe]:checked').val());
            console.log($('input[name=casier]:checked').val())
        } else {
            console.log("Les valeurs sont obligatoires");
        }

    });

});