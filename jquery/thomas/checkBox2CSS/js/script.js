$(document).ready(function () {
   $('input[type=checkbox]').each(function(){
      $(this).hide();
      $("<div class='custom_select'></div>").insertBefore($(this));
   });

    $('.custom_select').click(function() {
        // $(this).toggleClass('on').next().prop("checked", ! $(this).next().is(":checked"))
        var checkBox = $(this).next();
        if(checkBox.is(':checked')) {
            checkBox.prop("checked", false);
        } else {
            checkBox.prop("checked", true);
        }
        $(this).toggleClass("on");
    });


    $('input[type=submit]').click(function (e) {
       e.preventDefault(); // On arrête par défault le comportement du formulaire
        var res=[];
        //console.log(this);
        $('input[type=checkbox]:checked').each(function () {
            res.push($(this).val());
        });
        console.log(res);
    });
});