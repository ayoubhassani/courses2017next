console.log('in da house');

$(document).ready(
    function () {
        var div1 = $("#div1");
        var div2 = $(".truc");
        console.log(" 1 document.getElementById",document.getElementById("div1"));
        // Jquery renvoie tout le temps un tableau d'éléments
        console.log("Jquery by ID", div1);
        console.log("Jquery by Class", $(".truc"));
        console.log("Jquery by selector", $("body div"));

        div1.css("background-color", "lightblue");
        // div1.css("width", "200px");
        // div1.css("height", "200px");
        div1.height(230);
        div1.width(250);
        div2.css({'border-radius':"50%",
                'background-color':'lightsalmon',
                'height':'200px',
                'width': '200px'
        });

        $("ul li a").css("color", "orange");
        $("ul li:first-child a").css("color", "blue");

        $("h1").html("jQuery is in da house").css("color", "green");

        $("ul li").last().hide();
        $("ul li:nth-child(2)").next().css('color', 'red');
        $("ul li:nth-child(2)").next().next().show();
        $("ul li:nth-child(2)").find('a').click( function (event) {
            event.preventDefault();
            $("ul li:nth-child(2)").find('a').html("yahho");
            $('#link1').click();

        });

        //$("ul li:nth-child(2)").prev().html("GOOGLE").css("color", "black");

        console.log($("#div1 div:first-child").next('.test').css('background-color', 'red'));


        // L'évènement s'applique sur la collection
        $('#link1').click(function(event) {
            event.preventDefault();
            div2.slideToggle('slow', 'linear');
            if ($("h1").html() === "Clicked !"){
                $("h1").html("Jquery in da mornin");
                div1.hide("slow");
            } else {
                $("h1").eq(1).css('color', 'magenta');
                $("h1").html("Clicked !");
                div1.show("slow");
            }
        });

        function onMouseOver() {
            this.style.backgroundColor="pink";
        }

        function onMouseLeave() {
            this.style.backgroundColor="inherit";
        }

        function toggleThem(){
            $("ul li").eq(1).toggleClass("marron");
        }


        $("ul li").eq(1).mouseover(toggleThem);
        $("ul li").eq(1).mouseleave(toggleThem);
    }
);