$(document).ready(function () {
   $('input[type=checkbox]').each(function(){
      $(this).hide();
      $('<img src="img/off.png" class="customCheckBox">').insertBefore($(this));
   });

    $('.customCheckBox').click(function () {
        var checkBox = $(this).next();
        console.log(checkBox.is(':checked'));
        if(checkBox.is(':checked')) {
            checkBox.prop("checked", false);
            this.src = "img/off.png"; // En mode JavaScript
        } else {
            checkBox.prop("checked", true);
            $(this).attr('src',"img/on.png"); // En mode jQuery
        }
    });

    $('input[type=submit]').click(function (e) {
       e.preventDefault(); // On arrête par défault le comportement du formulaire
        var res=[];
        //console.log(this);
        $('input[type=checkbox]:checked').each(function () {
            res.push($(this).val());
        });
        console.log(res);
    });
});