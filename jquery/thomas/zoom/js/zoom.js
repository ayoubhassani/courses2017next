$(document).ready(function() {
    var Conf =  {
        wLoupe:200,
        hLoupe:200,
        loupeRadius:"50%"
    };
   //$(".zoom .zoom_big").hide();
    $(".zoom .zoom_small").css('cursor', 'zoom-in');
    var wSmall = $(".zoom .zoom_small").width();
    var hSmall = $(".zoom .zoom_small").height();
    var offsetTop = $(".zoom .zoom_small").offset().top;
    var offsetLeft = $(".zoom .zoom_small").offset().left;
    console.log("wSmall : " + wSmall, "Hsmall : "+ hSmall, "offsetTop : "+ offsetTop, "offsetLeft : ", offsetLeft);

    $("body").append("<div class='loupe'></div>");
    $('.loupe').css({
        "border-radius":Conf.loupeRadius,
        "width":Conf.wLoupe+"px",
        "height":Conf.hLoupe+"px",
        "border":"1px solid red",
        "position":"absolute",
        "background" : 'url('+ $(".zoom .zoom_big").attr("src")+')'
    });
    $(".loupe").hide();

    $(".zoom .zoom_small").mouseenter(function (){
        $('.loupe').show();
    }).mouseout( function () {
        $('.loupe').hide();
    }).mousemove(function(event) {
        var x = event.clientX - offsetLeft;
        var y = event.clientY - offsetTop;
        var coefX = (x / wSmall) * 100;
        var coefY = (y / hSmall) * 100;
        $(".loupe").css({
            "left":event.clientX-100+"px",
            "top":event.clientY+10+"px",
            'background-position': coefX+'% '+coefY+'%'
        });

    })
});