$(document).ready(function () {
    var Conf = {
        wLoupe:100,
        hLoupe:100,
        loupeRadius:"50%"
    };

   $('.zoom .zoom_big').hide() ;
   var Wsmall = $(".zoom .zoom_small").width();
   var Hsmall = $(".zoom .zoom_small").height();
   var offsetTop = $(".zoom .zoom_small").offset().top;
   var offsetLeft = $(".zoom .zoom_small").offset().left;

   console.log(
       "Wsmall :"+Wsmall,
       "Hsmall"+Hsmall,
       "offsetTop"+offsetTop,
       "offsetLeft"+offsetLeft
   );

   $('body').append("<div class='loupe'></div>");
   //configuration de la loupe, puis loupe cachée
   $(".loupe").css({
       "border-radius":Conf.loupeRadius,
       "width":Conf.wLoupe+"px",
       "height":Conf.hLoupe+"px",
       "border":"1px solid red",
       "position":"absolute",
       "background-image":'url('+ $(".zoom .zoom_big").attr("src")+')'
   });
    //$('.loupe').hide()
    $(".zoom .zoom_small").css('cursor','zoom-in');
    $(".zoom .zoom_small").mouseenter(function () {
        $('.loupe').show();
    });
    $(".zoom .zoom_small").mouseout(function () {
        $('.loupe').hide();
    });
    $(".zoom .zoom_small").mousemove(function (event) {
        var x = event.clientX - offsetLeft;
        var y = event.clientY - offsetTop;
        var coefX = (x /Wsmall)*100;
        var coefY = (y /Hsmall)*100;
        $(".loupe").css({
            'left':event.clientX+"px",
            'top':event.clientY+"px",
            'background-position':coefX+'%'+coefY+'%'
        });

    })

});