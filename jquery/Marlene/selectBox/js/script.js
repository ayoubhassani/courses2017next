function Ready() {

    $("select").hide();
    $("<div class='custom_select'>"+
        "<div class='btn'></div>"+
        "<div class='liste'></div>"+
        "</div>").insertBefore($("select"));
    $('.custom_select .liste').hide();
    $('.custom_select').each(function () {
        $(this).find('.btn').html($(this).next().find(":selected").html());
        var ul = "<ul>";
        $(this).next().find("option").each(function () {
            ul += "<li><a href='#' CustomVal='"+$(this).val()+"'>"+$(this).html()+"</a></li>"
        });
        ul += "<ul>";
        $(this).find('.liste').html(ul);

    });
    $('.custom_select .btn').click(function () {
        $('.custom_select .liste').hide();
        $(this).next().toggle();
    });

    $("a[CustomVal]").click(function () {
        var value = $(this).attr("CustomVal"),DisplayVal = $(this).html();
        $(this).parent().parent().parent().parent().next().val(value);
        $(this).parent().parent().parent().prev().html(DisplayVal);
        //toggle fait disparaitre la liste
        $(this).parent().parent().parent().toggle();
    });


    $("input[type=submit]").click(function (r) {
       r.preventDefault();
       $('select').each(function () {
           console.log($(this).val());
       })
    });
}

$(document).ready(Ready);
