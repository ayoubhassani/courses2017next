// $(document).ready(function () {
//     //on cache toutes les divs
//     $(".accordeon div").hide();
//     //ono deroule lentement la div du h2 actif
//     $("h2").first().addClass('active').next().show("slow");
//     $('h2').click(function () {
//         if($(this).hasClass('active')){
//             //click sur l'élément actif
//             $(this).next().hide("slow");
//             $(this).removeClass('active');
//         }else{
//             $("h2.active").removeClass('active').next().hide('slow');
//             $(this).addClass('active').next().show("slow");
//         }
//     });
// });

$(document).ready(function () {

    $('.accordeon div').slideUp();
    $('h2').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active').next().slideUp();
        } else {
            $('h2.active').removeClass('active').next().slideUp();
            $(this).next().slideToggle('slow');
            $(this).addClass('active');
        }

    })
    });
