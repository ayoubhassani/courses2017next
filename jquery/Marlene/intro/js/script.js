console.log('script loaded');

$(document).ready(
  function () {
      var div1 = $("#div1");
      var div2 = $ (".truc");
      console.log('document loaded');
      document.getElementById("div1");
      console.log('document.getElementById', document.getElementById("div1"));
      console.log('$ by id', div1);
      console.log('$ by class name', $(".truc"));
      console.log('$ by body div', $("body div"));
     // $("#div1");

      div1.css("background-color","cornflowerblue");
      //div1.css("height","100px");
      //div1.css("width","100px");
      //autre ecriture
      div1.height(200);
      div1.width(200);
      //passer plusieurs paramètres de style
      div2.css ({
          'border-radius':"50%",
          'height':"100px",
          'width':'100px',
          'border':'1px solid coral'
      });
      console.log(div1.height());
      console.log(div2.height());

      $("ul li a").css("color","coral");
      $("ul li:first-child a").css("color","cornflowerblue");

      //html remplace l'html dans l'element
      $("h1").html("jQuery intro").css('color','coral');
      console.log($("h1").html());

      //hide=>cache le dernier (last) element li
      $("ul li").last().hide();
      //show=>affiche l'element
     // $("ul li:nth-child(2)").next().css('color','yellow');
      $("ul li:nth-child(2)").next().next().show();
      $("ul li:nth-child(2)").next().find('a').css('color','yellow');
      $("ul li:nth-child(2)").next().find('a').css('color','yellow');
      //$("ul li:nth-child(2)").next().find('a').html('New Link Text');
      $("ul li:nth-child(2)").next().find('a').click(function (event) {
          event.preventDefault();
          $("ul li:nth-child(2)").next().find('a').html('New Link Text');
          $('#link1').click();
      });
      console.log($('#div1 div.first-child' ));
      $('#div1 div:first-child' ).next('.test').css('background-color','cornsilk');

      //fonction qui appelle une fonction qui va etre executée
      //preventDefault enleve la fonction de l'evenement par defaut
      $('#link1').click(function (event) {
          event.preventDefault();
          //sur le rond
          div2.slideToggle('slow');
          //slow se ferme apres le carre bleu
          //fast se ferme avant le carré bleu
          //div2.slideToggle('fast');
          if( $("h1").html()=="clicked !!!"){
              $('h1').eq(1).css("color",'coral');
              $("h1").html("jQuery intro");
              div1.show("slow");
          }else {
              $('h1').eq(1).css("color",'pink');
              $("h1").html("clicked !!!");
              $("#div1").hide("slow");
          }
      });
      function onMouseOver() {
          this.style.backgroundColor="lemonchiffon";
      }
      var onMouseLeave=function () {
          this.style.backgroundColor="inherit";
      };
      //activer les commentaires suivants pour activer la function on MouseOver
      //$("ul li").eq(1).mouseover(onMouseOver);
      //$("ul li").eq(1).mouseleave(onMouseLeave);

      function toggleThem() {
      $("ul li").eq(1).toggleClass('pink');
      }
      $("ul li").eq(1).mouseover(toggleThem);
      $("ul li").eq(1).mouseleave(toggleThem);

      var attr = $("ul li").first().find("a").attr('href');
      console.log(attr);
      $("ul li").first().find("a").attr('href', "http://gmail.com");
      var attr = $("ul li").first().find("a").attr('href');
      console.log(attr);
  });