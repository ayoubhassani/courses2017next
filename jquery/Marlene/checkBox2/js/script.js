$(document).ready(function () {
    $('input[type=checkbox]').each(function () {
        $(this).hide();
        $('<div class="custom_select"></div>').insertBefore($(this));

    });

    $('.custom_select').click(function () {
        var custom_select = $(this).next();
        if(custom_select.is(':checked')){
            custom_select.prop("checked", false);
        }else{
            custom_select.prop("checked", true);
        }
        $(this).toggleClass("on");

    });

    //ou
    // $('.custom_select').click(function() {
    //     $(this).toggleClass('on').next().prop('checked', ! $(this).is(':checked'));
    // });

    $('input[type=submit]').click(function (e) {
        e.preventDefault();
        var res = [];
        $('input[type=checkbox]:checked').each(function () {
            res.push($(this).val());
        });
        console.log(res);

    });
});