var apikey = "c959f299bea404067b6e04b7e5814e8b";
var TheMovieDbUrl = "https://api.themoviedb.org/3";
var TheMovieDbimgUrl = "https://image.tmdb.org/t/p/";
var TheMovieDblang = "fr-fr";
var TheMovieDbCurentPage = 1;
var sizes = ["w92", "w154", "w185", "w342", "w500", "w780"];

$(document).ready(function () {


    $('#search').autoComplete({
        url: TheMovieDbUrl + '/search/movie',
        apikey:"c959f299bea404067b6e04b7e5814e8b",
        nbChar:2,
        propertiesArrayDansData : "results",
        CompleteProperty:'original_title'},function(){
        return {
            api_key: conf.apikey,
            language: lang,
            query: encodeURIComponent( $('#search').val()),
            page: CurentPage
        }
    });


    $('#basket').droppable({
        tolerance:'touch',
        drop:function(event,ui){
            console.log( ui.helper.data);
            $('#basket').append('<p>'+ui.draggable.find("h4").html()+'</p>')
            $('#basket').find("#total").html(parseFloat($('#basket').find("#total").html())+ui.helper.data.price);
        }
    })
    $('#search').keyup(function(e){
        if(e.key=="Enter")$('#rechercher').click();
    });
    $('#next').click(function () {
        lastPage = TheMovieDbCurentPage;
        TheMovieDbCurentPage++;
        $('#rechercher').click()
    });
    $('#prev').click(function () {
        TheMovieDbCurentPage--;
        $('#rechercher').click()
    });
    $('#rechercher').click(function () {
        var titre = $('#search').val();
        if (titre != "") getSearchResult(titre);
    });
});

function getSearchResult(titre) {
    var data = {
        api_key: apikey,
        language: TheMovieDblang,
        query: titre,
        page: TheMovieDbCurentPage
    }
    $.ajax({
        type: "GET",
        url: TheMovieDbUrl + '/search/movie',
        data: data,
        success: function (result) {
            generateHtmlFor(result.results);
            console.log(result);
        }
    })
}

function generateHtmlFor(movies) {
    var html = '';
    $.each(movies, function (i, movie) {
      html += '<div Ayoub="movie" class="row" img="'+movie.poster_path +'" title="'+movie.original_title+'" price="'+movie.vote_average+'">' +
          '<div class="col-sm-4"><img  src="'+ TheMovieDbimgUrl + sizes[2] + movie.poster_path +'" alt="Card image cap"></div>' +
          '<div class="col-sm-8">' +
          '<h4>'+ movie.original_title + ' (' + movie.release_date + ') note: ' + movie.vote_average +'</h4> ' +
          '<p>'+ movie.overview +'</p> ' +
          '</div>' +
          '</div>'
        // html +='<div class="card" style="width: 342px;" movie-id="' + movie.id + '">' +
        // ' <img class="card-img-top" src="'+ TheMovieDbimgUrl + sizes[2] + movie.poster_path +'" alt="Card image cap"> ' +
        // '<div class="card-body"> ' +
        // '<h4 class="card-title">'+ movie.original_title + ' (' + movie.release_date + ') note: ' + movie.vote_average +'</h4> ' +
        // '<p class="card-text">'+ movie.overview +'</p> ' +
        // '<a href="#" class="btn btn-primary">View</a> ' +
        // '</div> ' +
        // '</div>';

    });
    $("#result").html(html);
    $("div.row").draggable({
        cursor:'move',
        helper:'clone',
        drag:function(event,ui){
            var img = ui.helper.attr('img');
            var titre = ui.helper.attr('title');
            var price = ui.helper.price =  ui.helper.attr('price');
                ui.helper.data ={
                imgIllustration :img,
                titre:titre,
                price:parseFloat(price)
            };

            var htmlDragg = '<div class="card" style="width: 10rem;"> <img class="card-img-top img-fluid"src="'+TheMovieDbimgUrl + sizes[0] +img+'" alt="Card image cap"> <div class="card-block"><p class="card-text card-title">'+titre+'</p></div ></div>';
var html = '<div>'+titre+'</div>'
            ui.helper.css('z-index','999').html(html);
                // ("<img class='img-thumbnail' src='"+TheMovieDbimgUrl + sizes[0] +img+"'>" +
                // "<div class='badge badge-info'> "+titre+"</div>") 1.333333 1,33
            // ui.helper.css('z-index','999').find("div.col-sm-8").hide();
        }
    });



}










