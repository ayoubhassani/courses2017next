/**
 * Created by stagiaire on 04/09/2017.
 */
// (function($){
//     $.fn.autosize = function (option) {
//     }
// })($)
function definitionDuModule($) {
$(document).click(function(){
    $('.CmpList').hide();
})
    $.fn.autoComplete = function (options) {
        // **this** ici est un object jQuery et qui represente la collection des elements retournées par le selecteur
        var defaults = {
            url: "",
            apikey: "Your Api Key HERE",
            nbChar: 2,
            propertiesArrayDansData: "results",
            CompleteProperty: 'name'
        };
        var conf = $.extend(defaults, options);

        var lang = "fr-fr";
        var CurentPage = 1;
        var sizes = ["w92", "w154", "w185", "w342", "w500", "w780"];
//Ajout de css
        var cssText = ".CmpList{ border-radius: 5px; border:1px solid #CCCCCC; padding:5px; position:absolute; background-color: #FFFFFF; box-sizing: border-box; } .CmpList ul{ margin:0; padding:0; list-style-type: none; } .CmpList ul li{ text-decoration: none; color: #000000; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; } .CmpList ul li:hover{ background-color: #EEEEEE; }"
var style = document.createElement('style');
        style.innerText = cssText;
        $('head').append(style);


        $("<div class='CmpList'></div>").insertAfter(this);

        $('.CmpList').hide();
        // pour implementer la completion
        this.attr('autocomplete',"off").keyup(function () {
            // var data = callback();
            var InputText = $(this);
            if ($(this).val().length >= conf.nbChar) {
                var data = {
                    api_key: conf.apikey,
                    language: lang,
                    query: $(this).val(),
                    page: CurentPage
                };
               // var data2 = fn();

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    data: data,
                    url: conf.url,
                    success: function (data) {
                        // **this** represente l'object jQuery Ajax
                        console.log(data);
                        var arrayData =data[conf.propertiesArrayDansData];
                        if(arrayData.length>0){
                            var ulLi ="<ul>";
                            $.each(arrayData.sort().slice(0,10),function(indice,eachOne){
                                ulLi +="<li >"+eachOne[conf.CompleteProperty]+"</li>"
                            });
                             ulLi +="</ul>";
                            InputText.next().html(ulLi).show();
                            InputText.next().css({
                                'z-index':"9999",
                                left: InputText.offset().left+200+'px',
                                'max-width':InputText.outerWidth()-200+"px",
                                'min-width':InputText.outerWidth()-200+"px"
                            });
                             //TODO: Ajouter le click sur les LI ici
                            $('.CmpList ul li').click(function(){
                                InputText.val($(this).html());
                                $(this).parent().parent().hide();
                            });
                        }else {
                            InputText.next().hide();
                        }

                        //ConstructCommunesList(data);
                    },
                    error: function (data) {
                        alert('error : ' + data.status_message);
                    }
                })

            } else {
                //aucune completion
                //hide de la liste
                $('.CmpList').hide();
            }
        });


        return this;
    }
}
definitionDuModule($)