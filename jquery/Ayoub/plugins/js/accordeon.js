/**
 * Created by stagiaire on 29/08/2017.
 */
(function(jQuery){
    jQuery.fn.accordeon=function(options){
        //this represente la collection d'elements roots pour chacun des accordeons dans la page
        var defaults = {};
        var conf = $.extend(defaults,options);

        var styleCssText = " h2.active{ background-color: #FFFFFF; color: #000000; } h2:hover{ cursor: pointer; }  h2{ background-color: #EEEEEE; border -radius: 5px; padding: 5px; border:1px solid #919191; margin: 2px 0; color: #919191; }  div{ position: relative; border -radius: 5px; border:1px solid #919191; padding: 5px;}";
        var s = document.createElement("style");
        s.innerText = styleCssText;
        $("head").append(s);
        this.find("div").slideUp();
        var collectionRoots = this;
        this.find("h2").click(function(){
            //this represente l'element H2 clické
            if($(this).hasClass('active')){
                $(this).removeClass("active").next().slideUp();
            }else{
                collectionRoots.find("h2.active").removeClass("active").next().slideUp();
                $(this).next().slideToggle("slow");
                $(this).addClass('active');
            }
        });
        return this;
    }
})(jQuery)