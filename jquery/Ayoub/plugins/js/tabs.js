/**
 * Created by stagiaire on 29/08/2017.
 */
(function ($) {
    $.fn.tabulate = function (options) {
        var defaults = {}
        var conf = $.extend(defaults, options);

//integration du css
        var tabsCssText = ".tabs ul{ margin:0; padding:0; list-style-type: none; } .tabs ul li{ display: inline-block; padding:10px; background-color: #EEEEEE; border:1px solid #CCCCCC; border-radius:5px 5px 0 0; } .tabs ul li a{ text-decoration: none; color:#CCCCCC; } .tabs div{ border:1px solid #CCCCCC; padding:10px; margin-top:-1px; border-radius: 0 5px 5px 5px; display: none; } .tabs ul li.active{ background-color: red; border-bottom:1px solid #FFFFFF;} .tabs ul li.active a{ color: #000000; } .tabs ul li:hover{ cursor:pointer; }";
        var s = document.createElement("style");
        s.innerText = tabsCssText;
        $("head").append(s);
        this.addClass('tabs');
        var collection = this;
        this.each(function(){
            $($(this).find("ul li").first().addClass('active').find('a').attr("href")).show()
        });
        this.find("ul li").click(function () {
            var idDiv = $(this).parent().parent().find("ul li.active").first().removeClass('active').find('a').attr("href")
            $(this).parent().parent().find(idDiv).hide();
            this.style.transition = "background-color 5s";
            $($(this).addClass('active').find('a').attr("href")).show();
        });
        return this;
    }
})($)