function FLUX() {
    new flux.slider('#slider',{
        width:700,
        autoplay:true,
        controls:true,
        pagination:true,
        transitions:['bars3d'],
        onTransitionEnd: function(data) {
            var img = data.currentImage;
            console.log(img);
        }
    });
    $('img').width(700);
}
function slik(){
    $("#slider").slick({
        // normal options...
        infinite: false,
        autoplay:true,
        speed:400,
        // the magic
        responsive: [{

            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                infinite: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                dots: true
            }
        }, {
            breakpoint: 300,
            settings: "unslick" // destroys slick

        }]
    });
}
$(document).ready(slik);