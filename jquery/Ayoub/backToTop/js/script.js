function backToTopPugin($){
    function _showHideOnScloll(start){
        if( $(window).scrollTop()> start ){
            $('#back_to_top').fadeIn("slow");
        }else{
            $('#back_to_top').fadeOut("slow");
        }
    }
    $.fn.backToTop = function(options){
        var defaults= {
            duration:500,
            start:80,
            position:'right'
        }
        var conf = $.extend(defaults,options);
        this.append('<div id="back_to_top">^</div>');
        $('#back_to_top').css(conf.position,"10px");
        _showHideOnScloll(conf.start);
        $(window).scroll(function () {
            _showHideOnScloll(conf.start);
        });
        $('#back_to_top').click(function(){
            var ratio = $(window).scrollTop()/$('body').height();
            console.log(jQuery.easing);
            $("html, body").animate({"scrollTop":0},ratio*conf.duration,"easeIn");
            // $("#wrapper").animate({"width":"60%"},conf.duration);
        });



        return this;
    }
}
backToTopPugin($);
$(document).ready(function(){
    $('body').backToTop({
        duration:2000,
        start:150,

    });
});