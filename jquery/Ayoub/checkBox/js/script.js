
function solution2(){
    $('input[type=checkbox]').each(function () {
        $(this).hide();
        $('<div class="custom_select"></div>').insertBefore($(this));
    });

    $('.custom_select').click(function(){
        $(this).toggleClass('on').next().prop("checked",! $(this).next().is(':checked'))
    });
    $('input[type=submit]').click(function(e){
        e.preventDefault();
        var res=[];
        $('input[type=checkbox]:checked').each(function(){
            res.push($(this).val());
        });
        console.log(res);
    });

}




function solution1(){
    $('input[type=checkbox]').each(function () {
        $(this).hide();
        $('<img src="img/off.png" class="customCheckBox">').insertBefore($(this));
    });
    $('.customCheckBox').click(function(){
        var checkBox = $(this).next();
        if(checkBox.is(':checked')){
            checkBox.prop("checked",false);
            this.src = "img/off.png";
        }else{
            checkBox.prop("checked",true);
            $(this).attr('src',"img/on.png");
        }
    });
    $('input[type=submit]').click(function(e){
        e.preventDefault();
        var res=[];
        // cosole.log(this);
        $('input[type=checkbox]:checked').each(function(index,val){
            res.push($(this).val());
        });
        console.log(res);
    });
}



$(document).ready(solution2);