$.getScript('js.js', function () {
    console.log(arguments);
});

$(document).ready(function () {
    $('.overlay').hide().append("<img src='img/loading_apple.gif'>");
    $.get("https://jsonplaceholder.typicode.com/posts", function (data, sucess, xmlHttpRequest) {
        console.log("shorthand ", data);
        for (var i = 0; i < data.length; i++) {
            $("<h2 class='title'>" + data[i].title + "</h2>" +
                "<div class='body'><p>" + data[i].body + "</p></div>" +
                "<button class='modif' id='" + data[i].id + "'>Modifier</button>"+
                "<button class='delete' id='" + data[i].id + "'>Supprimer</button>").appendTo($('#posts-Liste'))
        }
        $('button.modif').click(function () {
            //**this** represente l'element Javascript
            //**$(this)** represente l'element a sa forme jQuery
            var title = $(this).prev().prev().html();
            var body = $(this).prev().find('p').html();
            var id = this.id;
            $("textarea").val(body);
            $('input[type=text]').val(title);
            $("input[type=hidden]").val(id);
        });
        $('button.delete').click(function () {
            //**this** represente l'element Javascript
            //**$(this)** represente l'element a sa forme jQuery
            var id = this.id;
            var jBtn = $(this);
            if (confirm("Autoriser la Suppression ?")){
                $.ajax({
                    url: "https://jsonplaceholder.typicode.com/posts/" + id,
                    type: "DELETE",
                    success: function (data) {
                        console.log("Suppression Ok ",arguments);
                        jBtn.prev().remove();
                        jBtn.prev().remove();
                        jBtn.prev().remove();
                        jBtn.remove();
                    }
                })
            }
        });
    });


    $('#modif').click(function () {
        console.log($("input[type=hidden]").val());
        var id = $("input[type=hidden]").val();
        $.ajax({
            url: "https://jsonplaceholder.typicode.com/posts/" + id,
            type: "PUT",
            data: {
                id: id,
                title: $('input[type=text]').val(),
                body: $("textarea").val()
            },
            success: function (data) {
                    console.log(arguments)
                var btn = $("button[id="+data.id+"].modif");
                btn.prev().prev().html(data.title);
                btn.prev().find('p').html(data.body);
            }
        })

    });


    $('input[id=add]').click(function () {
        // $.post("https://jsonplaceholder.typicode.com/posts",function(){});
        $.ajax({
            url: "https://jsonplaceholder.typicode.com/posts",
            type: "POST",
            data: {
                title: $('input[type=text]').val(),
                body: $("textarea").val()
            }
            , success: function (data) {
                // console.log(arguments)
                $("<h2 class='title'>" + data.title + "</h2><div class='body'><p>" + data.body + "</p></div>").appendTo($('#posts-Liste'))
            }
        })
    });

    // $.ajax({
    //     type: 'GET',
    //     url: "https://jsonplaceholder.typicode.com/posts",
    //     success: function (data, sucess, xmlHttpRequest) {
    //         console.log(data);
    //         for (var i = 0; i < data.length; i++) {
    //         $("<h2 class='title'>"+data[i].title+"</h2><div class='body'><p>"+data[i].body+"</p></div>").appendTo($('#posts-Liste'))
    //         }
    //     },
    //     error: function () {
    //         //management Error
    //         alert("error");
    //     },
    //     beforeSend: function () {
    //         $('.overlay').show();
    //     }
    // })
    $(document).ajaxComplete(function () {
        $('.overlay').hide();
    });


});


//sucess(data,"sucess",new XMLHttpRequest())








