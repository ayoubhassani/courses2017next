$(document).ready(function () {
    $('#completion').hide();
    $('#commune').keyup(function () {
        //var commune = this.value;
        var cne = $(this).val();
        var url = "http://geo.api.gouv.fr/communes"
        if (cne != "") {
            $.ajax({
                type: "GET",
                dataType: "json",
                data: {nom: cne},
                url: url,
                success: function (data) {
                    //console.log(data);
                    ConstructCommunesList(data);
                },
                error: function () {
                    alert('error');
                }
            })
        }else{
            $('#completion').hide();
        }
    });

    function ConstructCommunesList(tabCommunes) {
        if(tabCommunes.length>0){
            var html = "<ul>";
            $.each(tabCommunes.sort().slice(0,10),function(index,element){
                  //**this** represente l'ement courant du tableau
                html +="<li prop=''>"+this.nom+" => "+this.codeDepartement+"</li>"
            });
            html += "</ul>";
            $('#completion').html(html);
            $('#completion').css({
                left: $('#commune').offset().left+'px',
                'max-width':$('#commune').outerWidth()+"px",
                'min-width':$('#commune').outerWidth()+"px"
            });
            $('#completion ul li').click(function(){
                var value = $(this).html().substring(0,$(this).html().lastIndexOf('=')-1);
                var value = $(this).html().split(' =')[0];


                $('#commune').val(value);
                $('#completion').hide();
            });
            $('#completion').show();
        }else $('#completion').hide();
    }
});