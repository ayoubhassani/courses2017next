function newOne() {
    $(".accordeon > div").slideUp();
    $("h2").click(function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active').next().slideUp();
        }else{
            $("h2.active").removeClass('active').next().slideUp();
            $(this).next().slideToggle("slow");
            $(this).addClass('active');
        }
    });
}
function old_solution() {
    $(".accordeon div").hide();
    $("h2").first().addClass('active').next().show("slow");
    $("h2").click(function () {
        if ($(this).hasClass('active')) {//ici je click sur l'element actif
            $(this).removeClass('active').next().hide("slow");
        } else {
            $("h2.active").removeClass('active').next().hide("slow");
            $(this).addClass('active').next().show("slow");
        }
    });
}
$(document).ready(newOne);
