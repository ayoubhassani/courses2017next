$(document).ready(function () {
    //each etant une boucle qui dit "pour tout"
   $('input[type=checkbox]').each(function () {
       //hide = cacher donc on cache l'élément de la checkbox
       $(this).hide();
       //On insert la class custom_select pour jouer avec le Toggle.
       $('<div class="custom_select"></div>').insertBefore($(this));
   });

   $('.custom_select').click(function (){
       var checkBox = $(this).next();
       if(checkBox.is(':checked')){
           checkBox.prop("checked", false);
       }else{
           checkBox.prop("checked", true);
       }
       //Si il est "on" c'est l'image cochée, sinon c'est la case par defaut
    $(this).toggleClass("on");
   });

   //Sinon l'équivalent en une ligne:
    /*$('.custom_select').click(function(){
        $(this).toggleClass('on').next().prop("checked",! $(this).next().is(':checked'))
    });*/

   $('input[type=submit]').click(function (e) {
       //e= l'evenement en cours et PreventDefault Stop l'evenement en cours
       e.preventDefault();
       var res=[];
       $('input[type=checkbox]:checked').each(function (index, val) {
           res.push($(this).val());
       });
       console.log(res);
   });





    //$('.custom_select').toggleClass("on")
});