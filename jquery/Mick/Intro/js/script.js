console.log('script loaded');

jQuery(document).ready(
    function () {
        var div1 = jQuery("#div1");
        var div2 = jQuery(".thing");
        console.log('document loaded');
        console.log("document.getElementById",document.getElementById("div1"));
        console.log("jQuery ById", div1); //izkarva tablo ,ako ne iskame tablo dobaviame [0]-ne e object
        console.log("jQuery byClassname", jQuery(".thing"));
        console.log("jQuery body div", jQuery("body div"));

        div1.css("background-color","darkred");
        //div1.css("height","100px");
        div1.height(200);
        div1.width(200);
        div2.css({'background-color':"blue",
                  // 'border-radius':"50%",
                  'height':"150px",
                  'width':"150px",
                  'margin-top':"50px"
        });
        //console.log(div1.height());
        // console.log(div2.height());

        $("ul li a").css('color',"darkred");
        $("ul li:first-child a").css('color',"blue");

        //.html -> vkarva neshto v html-a.Shte vkarata h1.Ako ima h1 go zamenia
        $("h1").html("jQuery Intro").css('color','green');

        console.log($("h1").html());

        //hide last element of li
        $("ul li").last().hide();
        //show the last element li
        $("ul li:nth-child(2)").next().next().show();
        $("ul li:nth-child(2)").next().next().css('background','yellow');
        $("ul li a:nth-child(2)").next().css('background','yellow');

        //3th link hides
        $("ul li:nth-child(2)").next().find('a').click(function (event) {
            event.preventDefault();
            $("ul li:nth-child(2)").next().find('a').html("New Link Text");
            $('#link1').click();
        });
        // $("ul li:nth-child(2)").prev().html("GOOGLE").css("color","black");

        //A $ sign to define/access jQuery
        console.log(jQuery("#div1 div:first-child"));
        $("#div1 div:first-child").next('.test').css('background-color','blue');

        //function when we click
        $('#link1').click(function (event) {
            event.preventDefault();
            div2.slideToggle('slow');
            if($("h1").html()== "clicked!!!"){
                $("h1").html("jQuery Intro!");

                div1.show("slow");
        }else {
                $("h1").eq(1).css("color","red");
                $("h1").html("clicked!!!");
                jQuery("#div1").hide("slow");
            }

        });

        function onMouseOver() {
           this.style.backgroundColor="pink";
        }

        var  onMouseLeave = function() {
            this.style.backgroundColor="inherit";
        }

        //on click change color to pink
        //.eq(1)-choose the second element from ul li
        //can add true or false after "pink".If it is true,if clicked will stay pink
        function toggleThem(e) {
            e.preventDefault();
            $("ul li").eq(1).toggleClass("pink");
        }

        $("ul li").eq(1).click(toggleThem);
        // $("ul li").eq(1).mouseleave(onMouseLeave);


        $("ul li").first().find("a").attr('href','http://gmail.com');
        var attr = $("ul li").first().find("a").attr('href');
        $("ul li").first().attr('Mick',"http://gmail.com");
        console.log($("ul li").first().attr('Mick'))
    }


);