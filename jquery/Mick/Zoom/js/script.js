$(document).ready(function () {
    var Conf = {
        wloupe:100,
        hloupe:100,
        loupeRadius: "10%"
    };

    $(".zoom .zoom_big").hide();
    $(".zoom .zoom_small").css('cursor',"zoom-in");
    var Wsmall = $(".zoom .zoom_small").width();
    var Hsmall = $(".zoom .zoom_small").height();
    var offsetTop = $(".zoom .zoom_small").offset().top;
    var offsetleft =$(".zoom .zoom_small").offset().left;

    console.log("Wsmall : "+Wsmall,
    "Hsmall : "+Hsmall,
    "offsetTop : "+offsetTop,
    "offsetleft : "+offsetleft
    );

    $("body").append("<div class='loupe'></div>");
    $('.loupe').css({
        "border-radius" : Conf.loupeRadius,
        width: Conf.wloupe+'px',
        height: Conf.hloupe+'px',
        border: "1px solid red",
        position: "absolute",
        "background-image":'url('+$(".zoom .zoom_big").attr("src")+')'
    }).hide()

    //$('.loupe').hide()
    $(".zoom .zoom_small").mouseenter(function () {
        $('.loupe').show();
    });
    $(".zoom .zoom_small").mouseout(function () {
        $('.loupe').hide();
    });
    $(".zoom .zoom_small").mousemove(function () {
        var x = event.clientX - offsetleft;
        var y = event.clientY - offsetTop;
        var coefX= (x / Wsmall)*100;
        var coefY= (y / Hsmall)*100;
        $(".loupe").css({
            'left':event.clientX-40+"px",
            'top':event.clientY+5+"px",
            'background-position': coefX+'%'+coefY+'%'

        });

    });


});