$(document).ready(function () {

    var ConfLoupe = {
        wLoupe:150,
        hLoupe:150,
        loupeRadius:"10%"
    };

    $(".zoom .zoom_big").hide();
    $(".zoom .zoom_small").css('cursor',"zoom-in")
    var wSmall = $(".zoom .zoom_small").width();
    var hSmall = $(".zoom .zoom_small").height();
    var offSetTop = $(".zoom .zoom_small").offset().top;
    var offSetLeft = $(".zoom .zoom_small").offset().left;

    // console.log("wSmall : "+wSmall,
    //     "hSmall : "+hSmall,
    //     "offSetTop : "+offSetTop,
    //     "offSetLeft : "+offSetLeft
    // );

    $("body").append("<div class='loupe'></div>");
    $(".loupe").css({
        "border-radius":ConfLoupe.loupeRadius,
        "width":ConfLoupe.wLoupe +'px',
        "height":ConfLoupe.hLoupe +'px',
        "border" : "1px solid red",
        "position":"absolute",
        "background-image":'url('+$(".zoom .zoom_big").attr("src")+')'
    }).hide();
    // $('.loup').hide() sashtoto e kato ako dobavime .hide()

    $(".zoom .zoom_small").mouseenter(function() {
        $('.loupe').show();
    });

    $(".zoom .zoom_small").mouseout(function () {
        $('.loupe').hide();
    });


    $(".zoom .zoom_small").mousemove(function(event) {
        var x = event.clientX - offSetLeft;
        var y = event.clientY - offSetTop;
        var coefX = (x/wSmall)*100;
        var coefY = (y/hSmall)*100;
        $(".loupe").css({
            'left': event.clientX-67+"px",
            'top' : event.clientY+5 +"px",
            'background-position':coefX+'%'+coefY+'%',
        });


    });
});