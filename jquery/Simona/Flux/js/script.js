// https://plugins.jquery.com/tag/slider/


$(document).ready(
    function() {
        new flux.slider('#slider', {
            autoplay: true,
            controls: true,
            pagination: false,
            slideWidth: 800,
            adaptiveHeight: true,
            transitions: ['bars3d']

        });

    }
);

