$(document).ready(function () {
   $('input[type = checkbox]').each(function () {
       $(this).hide();
       $('<img src="img/off.png" class="customCheckBox">').insertBefore($(this));
   });

    $('.customCheckBox').click(function () {
       var checkBox =  $(this).next();
       console.log(checkBox.is(':checked'));
       if(checkBox.is(':checked')){
           checkBox.prop("checked",false);
           this.src = "img/off.png"; //Javascript writing
       }else{
           //prop -> everything that we put in css with ":",like ":hover" ,":clicked"
           checkBox.prop("checked",true);
          $(this).attr('src',"img/on.png") ; //jQuery writing
       }

    });

    $('input[type = submit]').click(function(e) {
        e.preventDefault();
        var res = [];
        console.log(this);
        $('input[type = checkbox]:checked').each(function() {
            console.log(this);
           res.push($(this).val());
        });
        console.log(res);
    });




});