// https://github.com/kenwheeler/slick/

$(document).ready(
    function Slick() {
        $("#slider").slick({

            // normal options...
            infinite: false,
            autoplay:true,
            speed:400,

            // the magic
            responsive: [{

                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    infinite: true
                }

            }, {

                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    dots: true
                }

            }, {

                breakpoint: 300,
                settings: "unslick" // destroys slick

            }]
        });
    }
);
