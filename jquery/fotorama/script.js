$(function () {
    // 1. Initialize fotorama manually.
    var $fotoramaDiv = $('#fotorama').fotorama();

    // 2. Get the API object.
    var fotorama = $fotoramaDiv.data('fotorama');

    // 3. Inspect it in console.
    console.log(fotorama);
});



$('.fotorama').fotorama({
    width: 700,
    maxwidth: '100%',
    ratio: 16/9,
    allowfullscreen: true,
    nav: 'thumbs'
});