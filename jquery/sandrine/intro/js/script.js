console.log('in da house');

$(document).ready(
    function () {
        var div1 = $("#div1");
        var div2 = $(".truc");
        console.log(" 1 document.getElementById",document.getElementById("div1"));
        // Jquery renvoie tout le temps un tableau d'éléments
        console.log("Jquery by ID", div1);
        console.log("Jquery by Class", $(".truc"));
        console.log("Jquery by selector", $("body div"));

        div1.css("background-color", "lightblue");
        // div1.css("width", "200px");
        // div1.css("height", "200px");
        div1.height(230);
        div1.width(250);
        div2.css({'border-radius':"50%",
            'background-color':'lightsalmon',
            'height':'200px',
            'width': '200px'
        });

        $("ul li a").css("color", "orange");
        $("ul li:first-child a").css("color", "blue");

        $("h1").html("jQuery is in da house").css("color", "green");

        $("ul li").last().hide();
        $("ul li:nth-child(2)").next().css('color', 'red');
        $("ul li:nth-child(2)").next().next().show();
        $("ul li:nth-child(2)").next().find('a').click(function (e) {
            e.preventDefault();                               // annule l'évènement par défaut du clic
            $("ul li:nth-child(2)").next().find('a').html('New link text');
            $('#link1').click();
        });

        // $("ul li:nth-child(2)").prev().html("GOOGLE").css("color", "black");

        console.log($("#div1 div:first-child").next('.test').css('background-color', 'red'));

        $('#link1').click(function (e) {
            e.preventDefault();             // annule l'évènement par défaut du clic (lien a href vers google)
            div2.slideToggle("slow");
            if($('h1').html() == 'clicked !'){  // et le remplace par la modif du h1
                $('h1').html('jQuery Intro');
                div1.hide('slow');
            } else {
                $('h1').eq(1).css('color','red');     // eq(1) = récupère le 2nd élément du tableau de h1
                $('h1').html('clicked !');
                div1.show('slow');
            }
        });

        function onMouseOver(e) {
            e.preventDefault();
            this.style.backgroundColor = "pink";
        }

        var onMouseLeave = function (e) {
            e.preventDefault();
            this.style.backgroundColor = "inherit";
        };

        // $("ul li").eq(1).mouseover(onMouseOver);
        // $("ul li").eq(1).mouseleave(onMouseLeave);

        // ou bien

        function toggleThem() {
            $("ul li").eq(1).toggleClass('pink');
        }

        $("ul li").eq(1).mouseover(toggleThem);
        $("ul li").eq(1).mouseleave(toggleThem);

        var attr = $('ul li').first().find('a').attr('href');
        // console.log(attr);
        $('ul li').first().find('a').attr('href', 'http://gmail.com');

        var attr = $('ul li').first().find('a').attr('href');
        console.log(attr);

        $('ul li').first().attr('ayoub', 'http://gmail.com');
        console.log($('ul li').first().attr('ayoub'));
    }




);