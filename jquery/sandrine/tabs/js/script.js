$(document).ready(function () {
    var attrHref = $("#tabs ul li").first().addClass('active').find('a').attr('href');
    $(attrHref).show();
    $('#tabs ul li').click(function () {
        $($("#tabs ul li.active").removeClass('active').find('a').attr('href')).hide();
        $($(this).addClass('active').find('a').attr('href')).slideDown();
    })
});