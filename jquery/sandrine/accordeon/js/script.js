$(document).ready(function () {
    // on cache toutes les div de la classe accordeon
    // $('.accordeon div').hide();

    // on rend actif le 1er h2 et on affiche sa div
   // $('h2').first().addClass('active').next().show('slow');

    // à chaque clic sur un h2
    // $('h2').click(function () {
    //     if($(this).hasClass('active')){
    //         // clic sur l'élément actif
    //         $(this).removeClass('active').next().hide('slow');
    //     } else {
    //         // clic sur un élément inactif
    //         $('h2.active').removeClass('active').next().hide('slow');
    //         $(this).addClass('active').next().show('slow');
    //     }
    // });

    // autre solution :
    $('.accordeon div').slideUp();
    $('h2').click(function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active').next().slideUp();
        } else {
            $('h2.active').removeClass('active').next().slideUp();
            $(this).next().slideToggle('slow');
            $(this).addClass('active');
        }
    })
});


