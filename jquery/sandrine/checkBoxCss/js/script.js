$(document).ready(function () {
    $('div.custom_select').hide();
    $('input[type=checkbox]').each(function () {
       $(this).hide();
       $('<div class="custom_select"></div>').insertBefore($(this));
    });

    $('.custom_select').click(function () {
        // var checkBox = $(this).next();
        // console.log(checkBox.val());
        // if(checkBox.is(':checked')){
        //     checkBox.prop("checked",false);
        // }else{
        //     checkBox.prop("checked",true);
        // }
        // $(this).toggleClass('on');

        //ou bien
        $(this).toggleClass('on').next().prop("checked", !$(this).next().is(':checked'));

    });

    $('input[type=submit]').click(function (e) {
        e.preventDefault();     // on arrete le processus naturel du submit
        var res = [];
        $('input[type=checkbox]:checked').each(function () {
           res.push($(this).val());
        });
        console.log(res);

    })
});