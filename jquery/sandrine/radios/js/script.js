$(document).ready(function () {
    $('input[type=radio]').each(function () {
        console.log(this);
        $(this).hide();
        $('<img src="img/radio_off.png" class="customRadio">').insertBefore($(this));
    });

    $('.customRadio').click(function () {
        var name = $(this).next().attr("name");
        $("input[name="+name+"]:checked").prev().attr('src',"img/radio_off.png");
        var radio = $(this).next();
        if(radio.is(':checked')){
            radio.prop("checked",false);
            this.src = "img/radio_off.png";                   //Javascript
        }else{
            radio.prop("checked",true);
            $(this).attr('src', "img/radio_on.png");          //jQuery
        }
    });

    $('input[type=submit]').click(function (e) {
        e.preventDefault();     // on arrete le processus naturel du submit
        console.log($("form")[0].checkValidity());
        var res = [];
        if(($('input[name=sexe]:checked').length == 1) && ($('input[name=casier]:checked').length == 1)){
        $("input[type=radio]:checked").each(function () {    // on ne prend que les valeurs des cases cochées
            // console.log(this);  // checkbox
            res.push($(this).val());
        });
        console.log($('input[name=sexe]:checked').val());
        console.log($('input[name=casier]:checked').val());
        console.log(res);}
        else {
            alert("les valeurs sont obligatoires");
        }
    });
});