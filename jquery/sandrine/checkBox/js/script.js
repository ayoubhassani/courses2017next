$(document).ready(function () {
    $('input[type=checkbox]').each(function () {
       $(this).hide();
       $('<img src="img/off.png" class="customCheckBox">').insertBefore($(this));
    });

    $('.customCheckBox').click(function () {
        var checkBox = $(this).next();
        console.log(checkBox.val());
        if(checkBox.is(':checked')){
            checkBox.prop("checked",false);
            this.src = "img/off.png";                   //Javascript
        }else{
            checkBox.prop("checked",true);
            $(this).attr('src', "img/on.png");          //jQuery
        }
    });

    $('input[type=submit]').click(function (e) {
        e.preventDefault();     // on arrete le processus naturel du submit
        var res = [];
        // console.log(this);      // submit
        $('input[type=checkbox]:checked').each(function () {    // on ne prend que les valeurs des cases cochées
            // console.log(this);  // checkbox
           res.push($(this).val());
        });
        console.log(res);

    })
});