$(document).ready(function () {
    var conf = {
        wLoupe:100,
        hLoupe:100,
        loupeRadius:'50%'
    };
    $('.zoom .zoom_big').hide();
    $('.zoom .zoom_small').css('cursor','zoom-in');
    var wSmall = $('.zoom .zoom_small').width();
    var hSmall = $('.zoom .zoom_small').height();
    var offsetTop = $('.zoom .zoom_small').offset().top;
    var offsetLeft = $('.zoom .zoom_small').offset().left;
    console.log('wSmall '+wSmall,'hSmall '+hSmall+' offsetTop '+offsetTop+' offsetLeft '+offsetLeft);
    $('body').append("<div class='loupe'></div>");
    // on configure la loupe
    $('.loupe').css({
        "border-radius":conf.loupeRadius,
        "width":conf.wLoupe+'px',
        "height":conf.hLoupe+'px',
        "border":"1px solid red",
        "position":"absolute",
        "background-image":'url('+$(".zoom .zoom_big").attr("src")+')'
    });
    //on cache la loupe
    $('.loupe').hide();

    $(".zoom .zoom_small").mouseenter(function () {
        $('.loupe').show();
    });
    $(".zoom .zoom_small").mouseout(function () {
        $('.loupe').hide();
    });
    $(".zoom .zoom_small").mousemove(function (e) {
        var x = e.clientX - offsetLeft;
        var y = e.clientY - offsetTop;
        var coefX = (x / wSmall)*100;
        var coefY = (y / hSmall)*100;
        $('.loupe').css({
            'left':e.clientX-40+'px',
            'top':e.clientY+5+'px',
            'background-position':coefX+'% '+coefY+'%'
        });


    });




});